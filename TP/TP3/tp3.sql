-- 2.1. Commencez par supprimer les 4 tables temporaires "..._pnc". --

DROP TABLE coureur_pnc;
DROP TABLE etape_pnc;
DROP TABLE participer_pnc;
DROP TABLE resultatetape_pnc;

-- 2.2. Créez la table "contratcoureur" en vous appuyant sur l’extrait du schéma relationnel --
--      ci-dessus et en respectant le dictionnaire des données suivant :                     --

CREATE TABLE contratcoureur
(
    PRIMARY KEY (idCoureur, idSaison),
    idCoureur  CHAR(11),
    idSaison INT(4),
    idEquipe CHAR(3));

ALTER TABLE contratcoureur
    ADD FOREIGN KEY (idCoureur) REFERENCES coureur(codeuci);

ALTER TABLE contratcoureur
    ADD FOREIGN KEY (idEquipe) REFERENCES equipe(idequipe);

-- 2.3. Insérez les données concernant les contrats de coureurs (c’est-à-dire la saison en --
--      cours "2011", le coureur et l’équipe à laquelle il appartient actuellement) dont   --
--      vous disposez dans la table "coureur".                                             --

INSERT INTO contratcoureur (idCoureur, idEquipe, idSaison)
SELECT  codeuci, idequipe, 2011 FROM coureur;

-- 2.4. Supprimez le champ "idEquipe" de la table coureur afin de respecter la dernière --
--      évolution de notre base "Un coureur appartient à une équipe pour une saison".   --

ALTER TABLE coureur
	DROP CONSTRAINT fk_equipe_coureur;

ALTER TABLE coureur
	DROP idequipe;

-- 2.5. Créez la table "contratdirecteur" en vous appuyant sur l’extrait du schéma relation- nel ci-dessus. --

CREATE TABLE contratdirecteur
(
    PRIMARY KEY(idEquipe, idSaison),
    idEquipe CHAR(3),
    idSaison INT(4),
    directeur varchar(30));
    
ALTER TABLE contratdirecteur
    ADD FOREIGN KEY (idEquipe) REFERENCES equipe(idequipe);

-- 2.6. Insérez les données concernant les contrats de directeurs (c’est-à-dire la saison en --
--      cours et l’équipe à laquelle il appartient actuellement) dont vous disposez dans la  --
--      table "equipe".                                                                      --

INSERT INTO contratdirecteur (idEquipe, idSaison, directeur)
SELECT  idequipe, 2011, directeur FROM equipe;

-- 2.7. Supprimez le champ "directeur" de la table equipe afin de respecter la dernière évolution --
--      de notre base "Un directeur est nommé dans une équipe pour une saison".                   --

ALTER TABLE equipe
        DROP directeur;

-- 3.1. Créez la vue "vue_abandonsPNC" avec les abandons du "Paris-Nice" --

CREATE VIEW vue_abandonsPNC 
AS SELECT r.idetape AS 'Etape', r.idCoureur AS 'Coureur'
FROM resultatetape r
WHERE r.idepreuve ='PNC' AND r.abandon =1;

-- 3.2. Créez la vue "vue_classementParEtapePNC" qui permet d’avoir le classement par --
--      étapes du "Paris-Nice" (avec le numéro d’étape, le classement, le code UCI,   --
--      le prénom, e nom et le temps (le résultat devra, bien entendu, être trié par  --
--      étape et par classement).                                                     --

CREATE VIEW vue_classementParEtapePNC AS SELECT
    r.idetape AS 'idEtape',
    r.classement AS 'classement',
    r.idCoureur AS 'code UCI',
    coureur.prenom AS 'prenom',
    coureur.nom AS 'nom',
    r.temps AS 'temps'
FROM
    resultatetape r
LEFT JOIN coureur ON r.idCoureur = coureur.codeuci
ORDER BY
    r.idetape ASC;

-- 3.3. Créez la vue "vue_classementParEtapePNCSansAbandons" qui permet d’avoir le --
--      classement par étape du Paris-Nice, en retirant des classements les        --
--      coureurs qui ont abandonné durant l’épreuve.                               --

CREATE VIEW vue_classementParEtapePNCSansAbandons
AS SELECT 
	r.idetape AS 'idEtape',
    r.classement AS 'classement',
    r.idCoureur AS 'code UCI',
    coureur.prenom AS 'prenom',
    coureur.nom AS 'nom',
    r.temps AS 'temps'
FROM resultatetape r
LEFT JOIN coureur ON r.idCoureur = coureur.codeuci
WHERE r.idepreuve ='PNC' AND r.abandon =0
ORDER BY
    r.idetape AND r.classement ASC;


-- 3.4. Créez la vue "vue_classementGeneralPNC" avec le classement général du Paris-Nice    --
--      (c’est-à-dire le classement à l’arrivée de la dernière étape avec le cumul de       --
--      tous les temps d’étapes) afin d’obtenir un résultat semblable au résultat suivant : --

CREATE VIEW vue_classementGeneralPNC
AS SELECT
    coureur.prenom AS 'Prénom',
    coureur.nom AS 'Nom',
    SEC_TO_TIME( SUM( TIME_TO_SEC(r.temps) ) ) AS 'Temps'
FROM resultatetape r
LEFT JOIN coureur ON r.idCoureur = coureur.codeuci
GROUP BY
	coureur.Nom
ORDER BY
    SEC_TO_TIME( SUM( TIME_TO_SEC(r.temps) ) ) ASC;

-- 3.5. Créez la vue "vue_classementGeneralPNCAvecEcarts" avec le classement général du --
--      Paris-Nice ainsi que les écarts sur le vainqueur afin d’obtenir un résultat     --
--      semblable au résultat suivant :                                                 --

CREATE VIEW vue_classementGeneralPNCAvecEcarts
AS SELECT
    coureur.prenom AS 'Prénom',
    coureur.nom AS 'Nom',
    SEC_TO_TIME( SUM( TIME_TO_SEC(r.temps) ) ) AS 'Temps'
FROM resultatetape r
LEFT JOIN coureur ON r.idCoureur = coureur.codeuci
GROUP BY
        coureur.Nom
WHERE r.idepreuve='PNC'
ORDER BY
    SEC_TO_TIME( SUM( TIME_TO_SEC(r.temps) ) ) ASC;

-- Sans les écarts --
