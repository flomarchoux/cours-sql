-- 2.1 Quel est le parcours du Tour de France 2011 ? --

SELECT `idetape` AS 'N° Étape', `villedepart` AS 'Ville de départ', `villearrivee`AS 'ville d\'arrivée', `distance` AS 'Distance en (km)' FROM `etape`;

-- 2.2 Quelles sont les villes qui, dans une même étape, sont "ville de départ" et "ville d’ar- rivée" ? --

SELECT villedepart AS 'Ville à la fois de épart et arrivée' FROM `etape` WHERE villedepart = villearrivee;

-- 2.3 Quelle est la distance totale du Tour de France 2011 ? --

SELECT SUM(distance) AS 'Distance totale' FROM `etape`;

-- 2.4 Quelle est la distance de l’étape la plus longue ? --

SELECT MAX(distance) AS 'Distance de l\'étape la plus longue' FROM `etape`;

-- 2.5 Quelle est la distance moyenne d’une étape ? --

SELECT AVG(distance) AS 'Distance moyene d\'une étape' FROM `etape`;

-- 2.6 Qui est parti avec le dossard n° 1 ? --

SELECT prenom AS 'Prénom', nom AS 'Nom' FROM `coureur` WHERE dossard = 1;

-- 2.7 Combien d’équipes participent au Tour 2011 ? --

SELECT COUNT(idequipe) AS 'Nombre d\'équipe participantes' FROM `equipe`;

-- 2.8 Quels sont les directeurs d’équipes françaises ? --

SELECT directeur AS 'Directeurs d\'équipe françaises' FROM `equipe` WHERE nationalite ='FRA';

-- 2.9 Quelles équipes ont un site internet en ".fr" ? --

SELECT nom AS 'Nom', url AS 'URL' FROM `equipe` WHERE url LIKE '%.fr';

-- 2.10 Quelle est la date de naissance du partant le plus âgé ? --

SELECT MIN(datedenaissance) AS 'Date de naissance du plus âgé' FROM `coureur`;

-- 2.11 Combien y a-t-il de nationalités représentées parmi les coureurs ? --

SELECT COUNT( DISTINCT nationalite) AS 'Nombre de nationalités représentées' FROM `coureur`

-- 2.12 Qui sont les vainqueurs d’étapes (triés par noms) ? --

SELECT DISTINCT c.prenom AS 'Prénom', c.nom AS 'Nom' 
FROM `resultatetape`
INNER JOIN coureur c ON c.dossard = resultatetape.idcoureur
WHERE resultatetape.classement = 1
ORDER BY c.nom ASC;

-- 2.13 À quelle étape s’est produit le premier abandon ? --

SELECT MIN(idetape) AS 'Étape du 1er abandon'
FROM `resultatetape` 
WHERE abandon != 0;

-- 2.14 Combien d’abandons y a-t-il eu au total lors du Tour de France 2011 ? --

SELECT COUNT(idetape) AS 'Nombre total d\'abandons'
FROM `resultatetape` 
WHERE abandon != 0;

-- 3.1. Quelles sont les villes qui sont à la fois "villes de départ" et "villes d’arrivée" ? --

SELECT villedepart AS 'Villes à la fois départs et arrivée' 
FROM `etape`
INTERSECT 
SELECT villearrivee AS 'Villes à la fois départs et arrivée'
FROM `etape`;

-- 3.2. Quelle est la date de naissance du partant français le plus âgé ? --

SELECT MIN(datedenaissance) AS 'Date de naissance du plus âgé' FROM `coureur` WHERE nationalite LIKE 'FRA';

-- 3.3. Combien y a-t-il de coureurs par pays représenté (affichage des pays ayant le plus   --
--      grand nombre de coureurs en premier et, en cas d’égalité, c’est l’ordre alphabétique --
--      sur le nom de pays qui prime) ?                                                      --

SELECT p.nom AS 'Pays', COUNT(nationalite) AS 'Nombre de coureurs' FROM `coureur`
INNER JOIN pays p
ON p.idpays = coureur.nationalite
GROUP BY coureur.nationalite
ORDER BY COUNT(nationalite) DESC

-- N'affiche pas dans l'ordre alphabétique en cas d'égalité --

-- 3.4. Quels sont les coureurs qui ont remporté plusieurs étapes ? --

