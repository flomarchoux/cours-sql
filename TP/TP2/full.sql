/*2.10.sql*/
﻿SELECT MIN(dateDeNaissance) AS "Date de naissance du plus âgé"
FROM coureur


/*2.11.sql*/
﻿SELECT COUNT(DISTINCT nationalite) AS "Nombre de nationalités représentées"
FROM coureur


/*2.12.sql*/
﻿SELECT DISTINCT prenom, nom
FROM coureur JOIN resultatEtape ON dossard = idCoureur
WHERE classement = 1
ORDER BY nom


/*2.13.sql*/
﻿SELECT MIN(idEtape) AS "Etape du 1er abandon"
FROM resultatetape
WHERE abandon = true


/*2.14.sql*/
﻿SELECT COUNT(*) AS "Nombre total d'abandon"
FROM resultatetape
WHERE abandon = true


/*2.1.sql*/
﻿SELECT idEtape AS "N°Étape", villeDepart AS "Ville de départ",
       villeArrivee AS "Ville d'arrivée", distance
FROM etape


/*2.2.sql*/
﻿SELECT villeDepart AS "Villes à la fois départ et arrivée"
FROM etape
WHERE villeDepart = villeArrivee


/*2.3.sql*/
﻿SELECT SUM(distance) AS "Distance totale"
FROM etape


/*2.4.sql*/
﻿SELECT MAX(distance) AS "Distance de l'étape la plus longue"
FROM etape


/*2.5.sql*/
﻿SELECT AVG(distance) AS "Distance moyenne d'une étape"
FROM etape


/*2.6.sql*/
﻿SELECT prenom, nom
FROM coureur
WHERE dossard = 1


/*2.7.sql*/
﻿SELECT COUNT(*) AS "Nombre d'équipes participantes"
FROM equipe


/*2.8.sql*/
﻿SELECT directeur AS "Directeur d'équipes françaises"
FROM equipe JOIN pays ON nationalite = idPays
WHERE pays.nom = 'France'


/*2.9.sql*/
﻿SELECT nom, url
FROM equipe
WHERE url LIKE '%.fr'


/*3.10.sql*/
﻿SELECT directeur AS "Directeur du vainqueur de la dernière étape"
FROM resultatEtape JOIN coureur ON idCoureur=dossard
                   JOIN equipe ON coureur.idEquipe = equipe.idEquipe
WHERE classement = 1
  AND idEtape = 21


/*3.11.sql*/
﻿SELECT equipe.nom AS "Equipe", AVG(temps) AS "Temps"
FROM resultatEtape JOIN coureur ON dossard = idCoureur
                   JOIN equipe ON coureur.idEquipe = equipe.idEquipe
WHERE idEtape = 2
GROUP BY coureur.idEquipe, equipe.nom
ORDER BY "Temps", "Equipe"


/*3.1.sql*/
﻿SELECT depart.villeDepart AS "Villes à la fois départ et arrivée"
FROM etape AS depart JOIN etape AS arrivee ON depart.villeDepart = arrivee.villeArrivee


/*3.2.sql*/
﻿SELECT MIN(dateDeNaissance) AS "Date de naissance du français le plus âgé"
FROM coureur JOIN pays ON nationalite = idPays
WHERE pays.nom = 'France'


/*3.3.sql*/
﻿SELECT pays.nom AS "Pays", COUNT(*) AS "Nombre de coureurs"
FROM coureur JOIN pays ON nationalite = idPays
GROUP BY pays.nom
ORDER BY "Nombre de coureurs" DESC, "PAYS"


/*3.4.sql*/
﻿SELECT prenom, nom
FROM coureur JOIN resultatEtape ON dossard = idcoureur
WHERE classement = 1
GROUP BY prenom, nom
HAVING COUNT(*) > 1


/*3.5.sql*/
﻿SELECT pays.nom AS "Pays ayant un seul coureur"
FROM coureur JOIN pays ON nationalite = idPays
GROUP BY pays.nom
HAVING COUNT(*) = 1
ORDER BY pays.nom


/*3.6.sql*/
﻿SELECT prenom, nom, (COUNT(*) * 8000) AS "Sommes acquises en victoires d'étapes"
FROM coureur JOIN resultatEtape ON dossard = idcoureur
WHERE classement = 1
GROUP BY idcoureur, prenom, nom
ORDER BY 3 DESC, nom


