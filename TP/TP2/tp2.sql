-- 2.1. L’équipe suivante n’est pas dans notre base (elle n’a pas participé au Tour de France). --
--      Ajoutez-la pour respecter la règle "Un coureur fait partie d’une équipe" (sans la-      --
--      quelle l’ajout d’un coureur de cette équipe est impossible) :                           --

INSERT INTO `equipe`(`idequipe`, `nom`, `directeur`, `url`, `nationalite`) 
VALUES ('BSC', 'BRETAGNE - SCHULLER', 'Emmanuel HUBERT', 'www.bretagne-schuller.fr', 'FRA');

-- 2.2. Les étapes du Paris-Nice ne figurent pas encore dans notre base de données. Ajou-   --
--      tez-les (bien entendu, l’ajout se fera en vous appuyant sur le contenu de la table  --
--      temporaire "etape_pnc", et surtout pas individuellement) pour respecter la règle    --
--      "Un coureur participe à des étapes" (sans laquelle l’ajout des résultats concernant --
--      ces étapes est impossible) :                                                        --

INSERT INTO etape (idepreuve, idetape, villedepart, villearrivee, distance)
SELECT  'PNC', idetape, villedepart, villearrivee, distance  FROM etape_pnc;

-- 2.3. La table temporaire "coureur_pnc" contient les coureurs engagés dans le Paris-Nice. --
--      Veillez à ce qu’ils figurent bien tous dans notre base de données :                 --

INSERT INTO coureur (codeuci, datedenaissance, idequipe, nationalite, nom, prenom)
SELECT  codeuci, datedenaissance, idequipe, nationalite, nom,  prenom FROM coureur_pnc
WHERE NOT EXISTS (SELECT codeuci FROM coureur WHERE coureur.codeuci = coureur_pnc.codeuci);

-- 2.4. La table temporaire "participer_pnc" contient les correspondances de dossards pour --
--      le Paris-Nice. Ajoutez ces informations à notre base de données :                  --

INSERT INTO participer (idepreuve, dossard, idcoureur)
SELECT 'PNC', dossard, idcoureur  FROM participer_pnc;

-- 2.5. La table temporaire "resultatetape" contient tous les résultats. Ajoutez-les à notre --
--      base de données :                                                                    --

INSERT INTO resultatetape (idepreuve, abandon, classement, idcoureur, idetape, temps)
SELECT 'PNC', abandon, classement, idcoureur, idetape, temps  FROM resultatetape_pnc;

-- 3.1. On ne va plus se servir des tables temporaires, supprimez donc leurs contenus. --

DELETE FROM coureur_pnc;
DELETE FROM etape_pnc;
DELETE FROM participer_pnc;
DELETE FROM resultatetape_pnc;

-- 3.2. Le coureur Luis León SANCHEZ (ESP19831124) ayant eu une crevaison dans les 3 der-    --
--      niers kilomètres4 se voit attribuer le même temps que le groupe auquel il apparte-   --
--      nait. Son classement ne change pas (168e) mais son temps doit donc être corrigé pour --
--      la valeur "05:00:56" :                                                               --

UPDATE `resultatetape` SET temps= "05:00:56"
WHERE idetape=2, 
idCoureur='ESP19831124', 
classement=168;

-- 3.3. Le coureur David MONCOUTIÉ (FRA19750430) n’est pas arrivé dernier de l’étape 2 --
--      puisqu’il a abandonné en cours d’étape (suite à une blessure de la veille) :   --

UPDATE `resultatetape` 
SET `classement`= NULL,`temps`= NULL,`abandon`=1 
WHERE `idCoureur`='FRA19750430' AND idepreuve=2;

-- 3.4. Le vainqueur de la 8e et dernière étape du Paris-Nice n’est pas Alberto CONTADOR    	  --
--      (il avait effectivement remporté le Paris-Nice en 2010, mais il n’y a pas participé 	  --
--      en 2011). Supprimez cette ligne erronée et corrigez l’intégralité du classement de  	  --
--      cette étape afin qu’entre autres, Thomas VOECKLER retrouve sa place de vainqueur d’étape. --

