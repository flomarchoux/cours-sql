--
-- 1) Retourner le prénom et la date d'embauche de tous les employés travaillant dans le département de Monsieur Chen, excepté lui-même.
--

SELECT first_name AS 'Prénom', last_name AS 'Nom', hire_date AS "Date d' embauche" 
FROM `employees` 
WHERE department_id = 
    (SELECT department_id 
    FROM employees 
    WHERE last_name = 'Chen') 
AND last_name <> 'Chen'

--
-- 2) Retourner le nom de famille et le salaire de tous les employés dont le salaire est supérieur au salaire moyen.
--

SELECT last_name AS 'Nom' , salary AS 'Salaire' FROM `employees` 
WHERE salary > 
    (SELECT AVG(salary) 
    FROM employees)

--
-- 3) Retourner le nom des départements dans lesquels au moins une personne dont le nom de famille commence par “A” travaille
--

SELECT department_name AS 'Nom département' 
FROM `departments` 
WHERE department_id IN 
    (SELECT department_id 
    FROM employees 
    WHERE last_name LIKE 'A%')

--
-- 4) Retourner le nom de famille des employés travaillant dans un département dont le location_id est 1700.
--

SELECT last_name AS 'Nom' 
FROM `employees` 
WHERE department_id IN 
    (SELECT department_id 
    FROM departments 
    WHERE location_id = 1700) 

--
-- 5) Retourner le nom de famille et le salaire des employés dont le manager est Steven King.
--

SELECT last_name AS 'Nom', salary AS 'Salaire' 
FROM employees
WHERE manager_id IN 
    (SELECT employee_id 
    FROM employees 
    WHERE last_name ='King' AND first_name='Steven')



