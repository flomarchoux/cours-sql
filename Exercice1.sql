CREATE DATABASE cours_sql;
USE cours_sql;

CREATE TABLE REGIONS (
    region_id INT PRIMARY KEY,
    region_name VARCHAR(100) NOT NULL
    );

CREATE TABLE COUNTRIES (
    country_id INT PRIMARY KEY,
    country_name VARCHAR(50) NOT NULL,
    region_id INT NOT NULL,
    FOREIGN KEY (region_id) REFERENCES REGIONS(region_id)
    );
    
CREATE TABLE LOCATIONS (
    location_id INT PRIMARY KEY,
    street_address VARCHAR(100) NOT NULL,
    postal_code INT NOT NULL,
    city VARCHAR(100) NOT NULL,
    state_province VARCHAR(50) NOT NULL,
    country_id INT NOT NULL,
    FOREIGN KEY (country_id) REFERENCES COUNTRIES(country_id)
    );
    
CREATE TABLE DEPARTMENTS (
    department_id INT PRIMARY KEY,
    department_name VARCHAR(50) NOT NULL,
    manager_id INT NOT NULL,
    location_id INT NOT NULL,
    FOREIGN KEY (location_id) REFERENCES LOCATIONS (location_id)
    );

CREATE TABLE JOBS (
    job_id INT PRIMARY KEY,
    job_title VARCHAR(50) NOT NULL,
    min_salary float,
    max_salary float
    );

CREATE TABLE JOB_GRADES (
    grade_level INT PRIMARY KEY,
    lowest_sal float,
    highst_sal float
    );

CREATE TABLE EMPLOYEES (
    employee_id INT PRIMARY KEY,
    department_id INT NOT NULL,
    job_id INT NOT NULL,
    manager_id INT,
    first_name VARCHAR(20) NOT NULL,
    last_name VARCHAR(20) NOT NULL,
    email VARCHAR(50) NOT NULL,
    phone_number INT(10),
    hire_date DATE NOT NULL,
    salary FLOAT,
    commission_pct FLOAT,
    FOREIGN KEY (department_id) REFERENCES DEPARTMENTS(department_id),
    FOREIGN KEY (job_id) REFERENCES JOBS(job_id)
    );

ALTER TABLE EMPLOYEES
    ADD FOREIGN KEY (manager_id) REFERENCES EMPLOYEES(employee_id);
   
CREATE TABLE JOB_HISTORY (
    start_date DATE NOT NULL,
    end_date DATE,
    department_id INT NOT NULL,
    job_id INT NOT NULL,
    employee_id INT NOT NULL,
    FOREIGN KEY (department_id) REFERENCES DEPARTMENTS(department_id),
    FOREIGN KEY (job_id) REFERENCES JOBS(job_id),
    FOREIGN KEY (employee_id) REFERENCES EMPLOYEES(employee_id)
    );
