# Cours SQL

## database_gen

Fichier contenant toutes les tables, ainsi que les INSERT (valeurs) qui permette de faire les exercices

---

## Exercice n°1

Création des tables et utilisation des PRIMARY KEY et des FOREIGN KEY

---

## Exercice n°2

Exercice sur les requêtes basé sur l'Exercice n°1

---

## Exercice n°3

Exercice sur les requêtes basé sur les données et les tables générées à partir du fichier database_gen.sql

---

## Exercice n°4

Exercice sur les requêtes 'JOIN' basé sur les données et les tables générées à partir du fichier database_gen.sql

---

## Exercice n°5

Exercice sur les requêtes 'avancées' basé sur les données et les tables générées à partir du fichier database_gen.sql

---

## Exercice n°6

Exercice sur les requêtes 'DML' basé sur les données et les tables générées à partir du fichier database_gen.sql

---
