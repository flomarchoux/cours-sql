--
-- 1) Ecrire une requête permettant de retourner le nom du manager de chaque département (table departments)
--

SELECT first_name, last_name, departments.department_name
FROM employees
INNER JOIN departments ON employees.employee_id = departments.manager_id

--
-- 2) Ecrire une requête permettant de savoir combien d'employés travaillent dans chaque département 
--    (récupérer le nom du département)
--

SELECT COUNT(employee_id), departments.department_name
FROM employees
INNER JOIN departments ON employees.department_id = departments.department_id
GROUP BY departments.department_id

--
-- 3) Ecrire une requête permettant de retourner nom de famille et prénom de chaque employé, ainsi que le nom 
--    de famille de son manager, en incluant l'employé 100 (qui n'a pas de manager)
--

SELECT employees.last_name AS 'Nom', employees.first_name AS 'Prénom', em.last_name
FROM `employees` 
LEFT OUTER JOIN departments ON departments.manager_id = employees.manager_id
LEFT OUTER JOIN employees em ON  em.employee_id = departments.manager_id

--
-- 4) Ecrire une requête permettant de retourner, pour chaque département, le nom du département et la ville 
--    dans laquelle il est situé
--

SELECT department_name, locations.city 
FROM departments 
INNER JOIN locations ON departments.location_id = locations.location_id

--
-- 5)  Ecrire une requête permettant de savoir combien d'employés travaillent dans la ville de Toronto
--

SELECT COUNT(employee_id) 
FROM employees e 
INNER JOIN departments d  ON d.department_id= e.department_id 
INNER JOIN locations l ON d.location_id =l.location_id 
WHERE l.city='Toronto'

--
-- 6) Ecrire une requête permettant de connaitre le nom et prénom de tous les employés qui sont arrivés dans 
--    l'entreprise avant leur propre manager
--

SELECT e.last_name AS 'Nom', e.first_name AS 'Prénom'
FROM employees e
LEFT OUTER JOIN departments ON departments.manager_id = e.manager_id
LEFT OUTER JOIN employees em ON  em.employee_id = departments.manager_id
WHERE e.hire_date<em.hire_date

--
-- 7) Retourner le numéro de département, le prénom et le nom du job des employés travaillant dans le 
--    département "Executive"
--

SELECT d.department_id,  e.first_name AS 'Prénom', j.job_title AS 'Job'
FROM employees e
INNER JOIN departments d ON e.department_id = d.department_id
INNER JOIN jobs j ON e.job_id = j.job_id
WHERE d.department_name = 'Executive'



