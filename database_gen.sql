DROP SCHEMA IF EXISTS hex_sql;
CREATE DATABASE hex_sql;
DROP USER IF EXISTS "hex_sql"@"localhost";
CREATE USER "hex_sql"@"localhost" IDENTIFIED BY "1234567890";
GRANT ALL PRIVILEGES ON hex_sql.* TO "hex_sql"@"localhost" WITH GRANT OPTION;
FLUSH PRIVILEGES;
use hex_sql;

/* TABLE REGIONS */
CREATE TABLE `regions`
(
    `region_id`   INT         NOT NULL AUTO_INCREMENT,
    `region_name` VARCHAR(25) NOT NULL,
    PRIMARY KEY (`region_id`)
) ENGINE = InnoDB;

/* TABLE COUNTRIES */
CREATE TABLE `countries`
(
    `country_id`   VARCHAR(2)  NOT NULL,
    `country_name` VARCHAR(40) NOT NULL,
    `region_id`    INT         NOT NULL,
    PRIMARY KEY (`country_id`)
) ENGINE = InnoDB;

/* TABLE LOCATIONS */
CREATE TABLE `locations`
(
    `location_id`    DECIMAL(4,0) NOT NULL,
    `street_address` VARCHAR(40)  NOT NULL,
    `postal_code`    VARCHAR(12),
    `city`           VARCHAR(30)  NOT NULL,
    `state_province` VARCHAR(25),
    `country_id`     VARCHAR(2)   NOT NULL,
    PRIMARY KEY (`location_id`)
) ENGINE = InnoDB;


/* TABLE DEPARTMENTS */
CREATE TABLE `departments`
(
    `department_id`   DECIMAL(4,0) NOT NULL,
    `department_name` VARCHAR(30)  NOT NULL,
    `manager_id`      DECIMAL(6,0),
    `location_id`     DECIMAL(4,0) NOT NULL,
    PRIMARY KEY (`department_id`)
) ENGINE = InnoDB;

/* TABLE JOBS */
CREATE TABLE `jobs`
(
    `job_id`     VARCHAR(10)  NOT NULL,
    `job_title`  VARCHAR(35)  NOT NULL,
    `min_salary` DECIMAL(6,0) NOT NULL,
    `max_salary` DECIMAL(6,0) NOT NULL,
    PRIMARY KEY (`job_id`)
) ENGINE = InnoDB;

/* TABLE EMPLOYEES */
CREATE TABLE `employees`
(
    `employee_id`    DECIMAL(6,0) NOT NULL,
    `first_name`     VARCHAR(20)  NOT NULL,
    `last_name`      VARCHAR(25)  NOT NULL,
    `email`          VARCHAR(25)  NOT NULL,
    `phone_number`   VARCHAR(20)  NOT NULL,
    `hire_date`      DATE         NOT NULL,
    `job_id`         VARCHAR(10)  NOT NULL,
    `salary`         DECIMAL(8,2) NOT NULL,
    `commission_pct` DECIMAL(2,2),
    `manager_id`     DECIMAL(6,0),
    `department_id`  DECIMAL(4,0),
    PRIMARY KEY (`employee_id`)
) ENGINE = InnoDB;

/* TABLE JOB_HISTORY */
CREATE TABLE `job_history`
(
    `employee_id`   DECIMAL(6,0) NOT NULL,
    `start_date`    DATE         NOT NULL,
    `end_date`      DATE         NOT NULL,
    `job_id`        VARCHAR(10)  NOT NULL,
    `department_id` DECIMAL(4,0) NOT NULL,
    PRIMARY KEY (`employee_id`, `start_date`)
) ENGINE = InnoDB;

/* TABLE JOB_GRADES */
CREATE TABLE `job_grades`
(
    `grade_level` VARCHAR(3) NOT NULL,
    `lowest_sal`  INT        NOT NULL,
    `highest_sal` INT        NOT NULL,
    PRIMARY KEY (`grade_level`)
) ENGINE = InnoDB;


Insert into regions values (1,'Europe');
Insert into regions values (2,'Americas');
Insert into regions values (3,'Asia');
Insert into regions values (4,'Middle East and Africa');

Insert into jobs values ('AD_PRES','President',20000,40000);
Insert into jobs values ('AD_VP','Administration Vice President',15000,30000);
Insert into jobs values ('AD_ASST','Administration Assistant',3000,6000);
Insert into jobs values ('FI_MGR','Finance Manager',8200,16000);
Insert into jobs values ('FI_ACCOUNT','Accountant',4200,9000);
Insert into jobs values ('AC_MGR','Accounting Manager',8200,16000);
Insert into jobs values ('AC_ACCOUNT','Public Accountant',4200,9000);
Insert into jobs values ('SA_MAN','Sales Manager',10000,20000);
Insert into jobs values ('SA_REP','Sales Representative',6000,12000);
Insert into jobs values ('PU_MAN','Purchasing Manager',8000,15000);
Insert into jobs values ('PU_CLERK','Purchasing Clerk',2500,5500);
Insert into jobs values ('ST_MAN','Stock Manager',5500,8500);
Insert into jobs values ('ST_CLERK','Stock Clerk',2000,5000);
Insert into jobs values ('SH_CLERK','Shipping Clerk',2500,5500);
Insert into jobs values ('IT_PROG','Programmer',4000,10000);
Insert into jobs values ('MK_MAN','Marketing Manager',9000,15000);
Insert into jobs values ('MK_REP','Marketing Representative',4000,9000);
Insert into jobs values ('HR_REP','Human Resources Representative',4000,9000);
Insert into jobs values ('PR_REP','Public Relations Representative',4500,10500);

Insert into employees values (100,'Steven','King','SKING','515.123.4567',str_to_date('17-JUN-87','%d-%b-%Y'),'AD_PRES',24000,null,null,90);
Insert into employees values (101,'Neena','Kochhar','NKOCHHAR','515.123.4568',str_to_date('21-SEP-89','%d-%b-%Y'),'AD_VP',17000,null,100,90);
Insert into employees values (102,'Lex','De Haan','LDEHAAN','515.123.4569',str_to_date('13-JAN-93','%d-%b-%Y'),'AD_VP',17000,null,100,90);
Insert into employees values (103,'Alexander','Hunold','AHUNOLD','590.423.4567',str_to_date('03-JAN-90','%d-%b-%Y'),'IT_PROG',9000,null,102,60);
Insert into employees values (104,'Bruce','Ernst','BERNST','590.423.4568',str_to_date('21-MAY-91','%d-%b-%Y'),'IT_PROG',6000,null,103,60);
Insert into employees values (105,'David','Austin','DAUSTIN','590.423.4569',str_to_date('25-JUN-97','%d-%b-%Y'),'IT_PROG',4800,null,103,60);
Insert into employees values (106,'Valli','Pataballa','VPATABAL','590.423.4560',str_to_date('05-FEB-98','%d-%b-%Y'),'IT_PROG',4800,null,103,60);
Insert into employees values (107,'Diana','Lorentz','DLORENTZ','590.423.5567',str_to_date('07-FEB-99','%d-%b-%Y'),'IT_PROG',4200,null,103,60);
Insert into employees values (108,'Nancy','Greenberg','NGREENBE','515.124.4569',str_to_date('17-AUG-94','%d-%b-%Y'),'FI_MGR',12000,null,101,100);
Insert into employees values (109,'Daniel','Faviet','DFAVIET','515.124.4169',str_to_date('16-AUG-94','%d-%b-%Y'),'FI_ACCOUNT',9000,null,108,100);
Insert into employees values (110,'John','Chen','JCHEN','515.124.4269',str_to_date('28-SEP-97','%d-%b-%Y'),'FI_ACCOUNT',8200,null,108,100);
Insert into employees values (111,'Ismael','Sciarra','ISCIARRA','515.124.4369',str_to_date('30-SEP-97','%d-%b-%Y'),'FI_ACCOUNT',7700,null,108,100);
Insert into employees values (112,'Jose Manuel','Urman','JMURMAN','515.124.4469',str_to_date('07-MAR-98','%d-%b-%Y'),'FI_ACCOUNT',7800,null,108,100);
Insert into employees values (113,'Luis','Popp','LPOPP','515.124.4567',str_to_date('07-DEC-99','%d-%b-%Y'),'FI_ACCOUNT',6900,null,108,100);
Insert into employees values (114,'Den','Raphaely','DRAPHEAL','515.127.4561',str_to_date('07-DEC-94','%d-%b-%Y'),'PU_MAN',11000,null,100,30);
Insert into employees values (115,'Alexander','Khoo','AKHOO','515.127.4562',str_to_date('18-MAY-95','%d-%b-%Y'),'PU_CLERK',3100,null,114,30);
Insert into employees values (116,'Shelli','Baida','SBAIDA','515.127.4563',str_to_date('24-DEC-97','%d-%b-%Y'),'PU_CLERK',2900,null,114,30);
Insert into employees values (117,'Sigal','Tobias','STOBIAS','515.127.4564',str_to_date('24-JUL-97','%d-%b-%Y'),'PU_CLERK',2800,null,114,30);
Insert into employees values (118,'Guy','Himuro','GHIMURO','515.127.4565',str_to_date('15-NOV-98','%d-%b-%Y'),'PU_CLERK',2600,null,114,30);
Insert into employees values (119,'Karen','Colmenares','KCOLMENA','515.127.4566',str_to_date('10-AUG-99','%d-%b-%Y'),'PU_CLERK',2500,null,114,30);
Insert into employees values (120,'Matthew','Weiss','MWEISS','650.123.1234',str_to_date('18-JUL-96','%d-%b-%Y'),'ST_MAN',8000,null,100,50);
Insert into employees values (121,'Adam','Fripp','AFRIPP','650.123.2234',str_to_date('10-APR-97','%d-%b-%Y'),'ST_MAN',8200,null,100,50);
Insert into employees values (122,'Payam','Kaufling','PKAUFLIN','650.123.3234',str_to_date('01-MAY-95','%d-%b-%Y'),'ST_MAN',7900,null,100,50);
Insert into employees values (123,'Shanta','Vollman','SVOLLMAN','650.123.4234',str_to_date('10-OCT-97','%d-%b-%Y'),'ST_MAN',6500,null,100,50);
Insert into employees values (124,'Kevin','Mourgos','KMOURGOS','650.123.5234',str_to_date('16-NOV-99','%d-%b-%Y'),'ST_MAN',5800,null,100,50);
Insert into employees values (125,'Julia','Nayer','JNAYER','650.124.1214',str_to_date('16-JUL-97','%d-%b-%Y'),'ST_CLERK',3200,null,120,50);
Insert into employees values (126,'Irene','Mikkilineni','IMIKKILI','650.124.1224',str_to_date('28-SEP-98','%d-%b-%Y'),'ST_CLERK',2700,null,120,50);
Insert into employees values (127,'James','Landry','JLANDRY','650.124.1334',str_to_date('14-JAN-99','%d-%b-%Y'),'ST_CLERK',2400,null,120,50);
Insert into employees values (128,'Steven','Markle','SMARKLE','650.124.1434',str_to_date('08-MAR-00','%d-%b-%Y'),'ST_CLERK',2200,null,120,50);
Insert into employees values (129,'Laura','Bissot','LBISSOT','650.124.5234',str_to_date('20-AUG-97','%d-%b-%Y'),'ST_CLERK',3300,null,121,50);
Insert into employees values (130,'Mozhe','Atkinson','MATKINSO','650.124.6234',str_to_date('30-OCT-97','%d-%b-%Y'),'ST_CLERK',2800,null,121,50);
Insert into employees values (131,'James','Marlow','JAMRLOW','650.124.7234',str_to_date('16-FEB-97','%d-%b-%Y'),'ST_CLERK',2500,null,121,50);
Insert into employees values (132,'TJ','Olson','TJOLSON','650.124.8234',str_to_date('10-APR-99','%d-%b-%Y'),'ST_CLERK',2100,null,121,50);
Insert into employees values (133,'Jason','Mallin','JMALLIN','650.127.1934',str_to_date('14-JUN-96','%d-%b-%Y'),'ST_CLERK',3300,null,122,50);
Insert into employees values (134,'Michael','Rogers','MROGERS','650.127.1834',str_to_date('26-AUG-98','%d-%b-%Y'),'ST_CLERK',2900,null,122,50);
Insert into employees values (135,'Ki','Gee','KGEE','650.127.1734',str_to_date('12-DEC-99','%d-%b-%Y'),'ST_CLERK',2400,null,122,50);
Insert into employees values (136,'Hazel','Philtanker','HPHILTAN','650.127.1634',str_to_date('06-FEB-00','%d-%b-%Y'),'ST_CLERK',2200,null,122,50);
Insert into employees values (137,'Renske','Ladwig','RLADWIG','650.121.1234',str_to_date('14-JUL-95','%d-%b-%Y'),'ST_CLERK',3600,null,123,50);
Insert into employees values (138,'Stephen','Stiles','SSTILES','650.121.2034',str_to_date('26-OCT-97','%d-%b-%Y'),'ST_CLERK',3200,null,123,50);
Insert into employees values (139,'John','Seo','JSEO','650.121.2019',str_to_date('12-FEB-98','%d-%b-%Y'),'ST_CLERK',2700,null,123,50);
Insert into employees values (140,'Joshua','Patel','JPATEL','650.121.1834',str_to_date('06-APR-98','%d-%b-%Y'),'ST_CLERK',2500,null,123,50);
Insert into employees values (141,'Trenna','Rajs','TRAJS','650.121.8009',str_to_date('17-OCT-95','%d-%b-%Y'),'ST_CLERK',3500,null,124,50);
Insert into employees values (142,'Curtis','Davies','CDAVIES','650.121.2994',str_to_date('29-JAN-97','%d-%b-%Y'),'ST_CLERK',3100,null,124,50);
Insert into employees values (143,'Randall','Matos','RMATOS','650.121.2874',str_to_date('15-MAR-98','%d-%b-%Y'),'ST_CLERK',2600,null,124,50);
Insert into employees values (144,'Peter','Vargas','PVARGAS','650.121.2004',str_to_date('09-JUL-98','%d-%b-%Y'),'ST_CLERK',2500,null,124,50);
Insert into employees values (145,'John','Russell','JRUSSEL','011.44.1344.429268',str_to_date('01-OCT-96','%d-%b-%Y'),'SA_MAN',14000,0.4,100,80);
Insert into employees values (146,'Karen','Partners','KPARTNER','011.44.1344.467268',str_to_date('05-JAN-97','%d-%b-%Y'),'SA_MAN',13500,0.3,100,80);
Insert into employees values (147,'Alberto','Errazuriz','AERRAZUR','011.44.1344.429278',str_to_date('10-MAR-97','%d-%b-%Y'),'SA_MAN',12000,0.3,100,80);
Insert into employees values (148,'Gerald','Cambrault','GCAMBRAU','011.44.1344.619268',str_to_date('15-OCT-99','%d-%b-%Y'),'SA_MAN',11000,0.3,100,80);
Insert into employees values (149,'Eleni','Zlotkey','EZLOTKEY','011.44.1344.429018',str_to_date('29-JAN-00','%d-%b-%Y'),'SA_MAN',10500,0.2,100,80);
Insert into employees values (150,'Peter','Tucker','PTUCKER','011.44.1344.129268',str_to_date('30-JAN-97','%d-%b-%Y'),'SA_REP',10000,0.3,145,80);
Insert into employees values (151,'David','Bernstein','DBERNSTE','011.44.1344.345268',str_to_date('24-MAR-97','%d-%b-%Y'),'SA_REP',9500,0.25,145,80);
Insert into employees values (152,'Peter','Hall','PHALL','011.44.1344.478968',str_to_date('20-AUG-97','%d-%b-%Y'),'SA_REP',9000,0.25,145,80);
Insert into employees values (153,'Christopher','Olsen','COLSEN','011.44.1344.498718',str_to_date('30-MAR-98','%d-%b-%Y'),'SA_REP',8000,0.2,145,80);
Insert into employees values (154,'Nanette','Cambrault','NCAMBRAU','011.44.1344.987668',str_to_date('09-DEC-98','%d-%b-%Y'),'SA_REP',7500,0.2,145,80);
Insert into employees values (155,'Oliver','Tuvault','OTUVAULT','011.44.1344.486508',str_to_date('23-NOV-99','%d-%b-%Y'),'SA_REP',7000,0.15,145,80);
Insert into employees values (156,'Janette','King','JKING','011.44.1345.429268',str_to_date('30-JAN-96','%d-%b-%Y'),'SA_REP',10000,0.35,146,80);
Insert into employees values (157,'Patrick','Sully','PSULLY','011.44.1345.929268',str_to_date('04-MAR-96','%d-%b-%Y'),'SA_REP',9500,0.35,146,80);
Insert into employees values (158,'Allan','McEwen','AMCEWEN','011.44.1345.829268',str_to_date('01-AUG-96','%d-%b-%Y'),'SA_REP',9000,0.35,146,80);
Insert into employees values (159,'Lindsey','Smith','LSMITH','011.44.1345.729268',str_to_date('10-MAR-97','%d-%b-%Y'),'SA_REP',8000,0.3,146,80);
Insert into employees values (160,'Louise','Doran','LDORAN','011.44.1345.629268',str_to_date('15-DEC-97','%d-%b-%Y'),'SA_REP',7500,0.3,146,80);
Insert into employees values (161,'Sarath','Sewall','SSEWALL','011.44.1345.529268',str_to_date('03-NOV-98','%d-%b-%Y'),'SA_REP',7000,0.25,146,80);
Insert into employees values (162,'Clara','Vishney','CVISHNEY','011.44.1346.129268',str_to_date('11-NOV-97','%d-%b-%Y'),'SA_REP',10500,0.25,147,80);
Insert into employees values (163,'Danielle','Greene','DGREENE','011.44.1346.229268',str_to_date('19-MAR-99','%d-%b-%Y'),'SA_REP',9500,0.15,147,80);
Insert into employees values (164,'Mattea','Marvins','MMARVINS','011.44.1346.329268',str_to_date('24-JAN-00','%d-%b-%Y'),'SA_REP',7200,0.1,147,80);
Insert into employees values (165,'David','Lee','DLEE','011.44.1346.529268',str_to_date('23-FEB-00','%d-%b-%Y'),'SA_REP',6800,0.1,147,80);
Insert into employees values (166,'Sundar','Ande','SANDE','011.44.1346.629268',str_to_date('24-MAR-00','%d-%b-%Y'),'SA_REP',6400,0.1,147,80);
Insert into employees values (167,'Amit','Banda','ABANDA','011.44.1346.729268',str_to_date('21-APR-00','%d-%b-%Y'),'SA_REP',6200,0.1,147,80);
Insert into employees values (168,'Lisa','Ozer','LOZER','011.44.1343.929268',str_to_date('11-MAR-97','%d-%b-%Y'),'SA_REP',11500,0.25,148,80);
Insert into employees values (169,'Harrison','Bloom','HBLOOM','011.44.1343.829268',str_to_date('23-MAR-98','%d-%b-%Y'),'SA_REP',10000,0.2,148,80);
Insert into employees values (170,'Tayler','Fox','TFOX','011.44.1343.729268',str_to_date('24-JAN-98','%d-%b-%Y'),'SA_REP',9600,0.2,148,80);
Insert into employees values (171,'William','Smith','WSMITH','011.44.1343.629268',str_to_date('23-FEB-99','%d-%b-%Y'),'SA_REP',7400,0.15,148,80);
Insert into employees values (172,'Elizabeth','Bates','EBATES','011.44.1343.529268',str_to_date('24-MAR-99','%d-%b-%Y'),'SA_REP',7300,0.15,148,80);
Insert into employees values (173,'Sundita','Kumar','SKUMAR','011.44.1343.329268',str_to_date('21-APR-00','%d-%b-%Y'),'SA_REP',6100,0.1,148,80);
Insert into employees values (174,'Ellen','Abel','EABEL','011.44.1644.429267',str_to_date('11-MAY-96','%d-%b-%Y'),'SA_REP',11000,0.3,149,80);
Insert into employees values (175,'Alyssa','Hutton','AHUTTON','011.44.1644.429266',str_to_date('19-MAR-97','%d-%b-%Y'),'SA_REP',8800,0.25,149,80);
Insert into employees values (176,'Jonathon','Taylor','JTAYLOR','011.44.1644.429265',str_to_date('24-MAR-98','%d-%b-%Y'),'SA_REP',8600,0.2,149,80);
Insert into employees values (177,'Jack','Livingston','JLIVINGS','011.44.1644.429264',str_to_date('23-APR-98','%d-%b-%Y'),'SA_REP',8400,0.2,149,80);
Insert into employees values (178,'Kimberely','Grant','KGRANT','011.44.1644.429263',str_to_date('24-MAY-99','%d-%b-%Y'),'SA_REP',7000,0.15,149,null);
Insert into employees values (179,'Charles','Johnson','CJOHNSON','011.44.1644.429262',str_to_date('04-JAN-00','%d-%b-%Y'),'SA_REP',6200,0.1,149,80);
Insert into employees values (180,'Winston','Taylor','WTAYLOR','650.507.9876',str_to_date('24-JAN-98','%d-%b-%Y'),'SH_CLERK',3200,null,120,50);
Insert into employees values (181,'Jean','Fleaur','JFLEAUR','650.507.9877',str_to_date('23-FEB-98','%d-%b-%Y'),'SH_CLERK',3100,null,120,50);
Insert into employees values (182,'Martha','Sullivan','MSULLIVA','650.507.9878',str_to_date('21-JUN-99','%d-%b-%Y'),'SH_CLERK',2500,null,120,50);
Insert into employees values (183,'Girard','Geoni','GGEONI','650.507.9879',str_to_date('03-FEB-00','%d-%b-%Y'),'SH_CLERK',2800,null,120,50);
Insert into employees values (184,'Nandita','Sarchand','NSARCHAN','650.509.1876',str_to_date('27-JAN-96','%d-%b-%Y'),'SH_CLERK',4200,null,121,50);
Insert into employees values (185,'Alexis','Bull','ABULL','650.509.2876',str_to_date('20-FEB-97','%d-%b-%Y'),'SH_CLERK',4100,null,121,50);
Insert into employees values (186,'Julia','Dellinger','JDELLING','650.509.3876',str_to_date('24-JUN-98','%d-%b-%Y'),'SH_CLERK',3400,null,121,50);
Insert into employees values (187,'Anthony','Cabrio','ACABRIO','650.509.4876',str_to_date('07-FEB-99','%d-%b-%Y'),'SH_CLERK',3000,null,121,50);
Insert into employees values (188,'Kelly','Chung','KCHUNG','650.505.1876',str_to_date('14-JUN-97','%d-%b-%Y'),'SH_CLERK',3800,null,122,50);
Insert into employees values (189,'Jennifer','Dilly','JDILLY','650.505.2876',str_to_date('13-AUG-97','%d-%b-%Y'),'SH_CLERK',3600,null,122,50);
Insert into employees values (190,'Timothy','Gates','TGATES','650.505.3876',str_to_date('11-JUL-98','%d-%b-%Y'),'SH_CLERK',2900,null,122,50);
Insert into employees values (191,'Randall','Perkins','RPERKINS','650.505.4876',str_to_date('19-DEC-99','%d-%b-%Y'),'SH_CLERK',2500,null,122,50);
Insert into employees values (192,'Sarah','Bell','SBELL','650.501.1876',str_to_date('04-FEB-96','%d-%b-%Y'),'SH_CLERK',4000,null,123,50);
Insert into employees values (193,'Britney','Everett','BEVERETT','650.501.2876',str_to_date('03-MAR-97','%d-%b-%Y'),'SH_CLERK',3900,null,123,50);
Insert into employees values (194,'Samuel','McCain','SMCCAIN','650.501.3876',str_to_date('01-JUL-98','%d-%b-%Y'),'SH_CLERK',3200,null,123,50);
Insert into employees values (195,'Vance','Jones','VJONES','650.501.4876',str_to_date('17-MAR-99','%d-%b-%Y'),'SH_CLERK',2800,null,123,50);
Insert into employees values (196,'Alana','Walsh','AWALSH','650.507.9811',str_to_date('24-APR-98','%d-%b-%Y'),'SH_CLERK',3100,null,124,50);
Insert into employees values (197,'Kevin','Feeney','KFEENEY','650.507.9822',str_to_date('23-MAY-98','%d-%b-%Y'),'SH_CLERK',3000,null,124,50);
Insert into employees values (198,'Donald','OConnell','DOCONNEL','650.507.9833',str_to_date('21-JUN-99','%d-%b-%Y'),'SH_CLERK',2600,null,124,50);
Insert into employees values (199,'Douglas','Grant','DGRANT','650.507.9844',str_to_date('13-JAN-00','%d-%b-%Y'),'SH_CLERK',2600,null,124,50);
Insert into employees values (200,'Jennifer','Whalen','JWHALEN','515.123.4444',str_to_date('17-SEP-87','%d-%b-%Y'),'AD_ASST',4400,null,101,10);
Insert into employees values (201,'Michael','Hartstein','MHARTSTE','515.123.5555',str_to_date('17-FEB-96','%d-%b-%Y'),'MK_MAN',13000,null,100,20);
Insert into employees values (202,'Pat','Fay','PFAY','603.123.6666',str_to_date('17-AUG-97','%d-%b-%Y'),'MK_REP',6000,null,201,20);
Insert into employees values (203,'Susan','Mavris','SMAVRIS','515.123.7777',str_to_date('07-JUN-94','%d-%b-%Y'),'HR_REP',6500,null,101,40);
Insert into employees values (204,'Hermann','Baer','HBAER','515.123.8888',str_to_date('07-JUN-94','%d-%b-%Y'),'PR_REP',10000,null,101,70);
Insert into employees values (205,'Shelley','Higgins','SHIGGINS','515.123.8080',str_to_date('07-JUN-94','%d-%b-%Y'),'AC_MGR',12000,null,101,110);
Insert into employees values (206,'William','Gietz','WGIETZ','515.123.8181',str_to_date('07-JUN-94','%d-%b-%Y'),'AC_ACCOUNT',8300,null,205,110);

Insert into countries values ('AR','Argentina',2);
Insert into countries values ('AU','Australia',3);
Insert into countries values ('BE','Belgium',1);
Insert into countries values ('BR','Brazil',2);
Insert into countries values ('CA','Canada',2);
Insert into countries values ('CH','Switzerland',1);
Insert into countries values ('CN','China',3);
Insert into countries values ('DE','Germany',1);
Insert into countries values ('DK','Denmark',1);
Insert into countries values ('EG','Egypt',4);
Insert into countries values ('FR','France',1);
Insert into countries values ('HK','HongKong',3);
Insert into countries values ('IL','Israel',4);
Insert into countries values ('IN','India',3);
Insert into countries values ('IT','Italy',1);
Insert into countries values ('JP','Japan',3);
Insert into countries values ('KW','Kuwait',4);
Insert into countries values ('MX','Mexico',2);
Insert into countries values ('NG','Nigeria',4);
Insert into countries values ('NL','Netherlands',1);
Insert into countries values ('SG','Singapore',3);
Insert into countries values ('UK','United Kingdom',1);
Insert into countries values ('US','United States of America',2);
Insert into countries values ('ZM','Zambia',4);
Insert into countries values ('ZW','Zimbabwe',4);

Insert into job_grades values ('A',1000,2999);
Insert into job_grades values ('B',3000,5999);
Insert into job_grades values ('C',6000,9999);
Insert into job_grades values ('D',10000,14999);
Insert into job_grades values ('E',15000,24999);
Insert into job_grades values ('F',25000,40000);

Insert into departments values (10,'Administration',200,1700);
Insert into departments values (20,'Marketing',201,1800);
Insert into departments values (30,'Purchasing',114,1700);
Insert into departments values (40,'Human Resources',203,2400);
Insert into departments values (50,'Shipping',121,1500);
Insert into departments values (60,'IT',103,1400);
Insert into departments values (70,'Public Relations',204,2700);
Insert into departments values (80,'Sales',145,2500);
Insert into departments values (90,'Executive',100,1700);
Insert into departments values (100,'Finance',108,1700);
Insert into departments values (110,'Accounting',205,1700);
Insert into departments values (120,'Treasury',null,1700);
Insert into departments values (130,'Corporate Tax',null,1700);
Insert into departments values (140,'Control And Credit',null,1700);
Insert into departments values (150,'Shareholder Services',null,1700);
Insert into departments values (160,'Benefits',null,1700);
Insert into departments values (170,'Manufacturing',null,1700);
Insert into departments values (180,'Construction',null,1700);
Insert into departments values (190,'Contracting',null,1700);
Insert into departments values (200,'Operations',null,1700);
Insert into departments values (210,'IT Support',null,1700);
Insert into departments values (220,'NOC',null,1700);
Insert into departments values (230,'IT Helpdesk',null,1700);
Insert into departments values (240,'Government Sales',null,1700);
Insert into departments values (250,'Retail Sales',null,1700);
Insert into departments values (260,'Recruiting',null,1700);
Insert into departments values (270,'Payroll',null,1700);

Insert into locations values (1000,'1297 Via Cola di Rie','00989','Roma',null,'IT');
Insert into locations values (1100,'93091 Calle della Testa','10934','Venice',null,'IT');
Insert into locations values (1200,'2017 Shinjuku-ku','1689','Tokyo','Tokyo Prefecture','JP');
Insert into locations values (1300,'9450 Kamiya-cho','6823','Hiroshima',null,'JP');
Insert into locations values (1400,'2014 Jabberwocky Rd','26192','Southlake','Texas','US');
Insert into locations values (1500,'2011 Interiors Blvd','99236','South San Francisco','California','US');
Insert into locations values (1600,'2007 Zagora St','50090','South Brunswick','New Jersey','US');
Insert into locations values (1700,'2004 Charade Rd','98199','Seattle','Washington','US');
Insert into locations values (1800,'147 Spadina Ave','M5V 2L7','Toronto','Ontario','CA');
Insert into locations values (1900,'6092 Boxwood St','YSW 9T2','Whitehorse','Yukon','CA');
Insert into locations values (2000,'40-5-12 Laogianggen','190518','Beijing',null,'CN');
Insert into locations values (2100,'1298 Vileparle (E)','490231','Bombay','Maharashtra','IN');
Insert into locations values (2200,'12-98 Victoria Street','2901','Sydney','New South Wales','AU');
Insert into locations values (2300,'198 Clementi North','540198','Singapore',null,'SG');
Insert into locations values (2400,'8204 Arthur St',null,'London',null,'UK');
Insert into locations values (2500,'Magdalen Centre, The Oxford Science Park','OX9 9ZB','Oxford','Oxford','UK');
Insert into locations values (2600,'9702 Chester Road','09629850293','Stretford','Manchester','UK');
Insert into locations values (2700,'Schwanthalerstr. 7031','80925','Munich','Bavaria','DE');
Insert into locations values (2800,'Rua Frei Caneca 1360 ','01307-002','Sao Paulo','Sao Paulo','BR');
Insert into locations values (2900,'20 Rue des Corps-Saints','1730','Geneva','Geneve','CH');
Insert into locations values (3000,'Murtenstrasse 921','3095','Bern','BE','CH');
Insert into locations values (3100,'Pieter Breughelstraat 837','3029SK','Utrecht','Utrecht','NL');
Insert into locations values (3200,'Mariano Escobedo 9991','11932','Mexico City','Distrito Federal,','MX');

Insert into job_history values (102,str_to_date('13-JAN-93','%d-%b-%Y'),str_to_date('24-JUL-98','%d-%b-%Y'),'IT_PROG',60);
Insert into job_history values (101,str_to_date('21-SEP-89','%d-%b-%Y'),str_to_date('27-OCT-93','%d-%b-%Y'),'AC_ACCOUNT',110);
Insert into job_history values (101,str_to_date('28-OCT-93','%d-%b-%Y'),str_to_date('15-MAR-97','%d-%b-%Y'),'AC_MGR',110);
Insert into job_history values (201,str_to_date('17-FEB-96','%d-%b-%Y'),str_to_date('19-DEC-99','%d-%b-%Y'),'MK_REP',20);
Insert into job_history values (114,str_to_date('24-MAR-98','%d-%b-%Y'),str_to_date('31-DEC-99','%d-%b-%Y'),'ST_CLERK',50);
Insert into job_history values (122,str_to_date('01-JAN-99','%d-%b-%Y'),str_to_date('31-DEC-99','%d-%b-%Y'),'ST_CLERK',50);
Insert into job_history values (200,str_to_date('17-SEP-87','%d-%b-%Y'),str_to_date('17-JUN-93','%d-%b-%Y'),'AD_ASST',90);
Insert into job_history values (176,str_to_date('24-MAR-98','%d-%b-%Y'),str_to_date('31-DEC-98','%d-%b-%Y'),'SA_REP',80);
Insert into job_history values (176,str_to_date('01-JAN-99','%d-%b-%Y'),str_to_date('31-DEC-99','%d-%b-%Y'),'SA_MAN',80);
Insert into job_history values (200,str_to_date('01-JUL-94','%d-%b-%Y'),str_to_date('31-DEC-98','%d-%b-%Y'),'AC_ACCOUNT',90);


ALTER TABLE `countries`
    ADD CONSTRAINT fk_to_region_id
    FOREIGN KEY (region_id)
    REFERENCES regions(region_id);

ALTER TABLE `departments`
    ADD CONSTRAINT fk_to_location_id
    FOREIGN KEY (location_id)
    REFERENCES locations(location_id);

ALTER TABLE `departments`
    ADD CONSTRAINT fk_to_manager_id2
    FOREIGN KEY (manager_id)
    REFERENCES employees(employee_id);

ALTER TABLE `employees`
    ADD CONSTRAINT fk_to_department_id
    FOREIGN KEY (department_id)
    REFERENCES departments(department_id);

ALTER TABLE `employees`
    ADD CONSTRAINT fk_to_job_id
    FOREIGN KEY (job_id)
    REFERENCES jobs(job_id);

ALTER TABLE `employees`
    ADD CONSTRAINT fk_to_manager_id
    FOREIGN KEY (manager_id)
    REFERENCES employees(employee_id);

ALTER TABLE `job_history`
    ADD CONSTRAINT fk_to_department_id2
    FOREIGN KEY (department_id)
    REFERENCES departments(department_id);

ALTER TABLE `job_history`
    ADD CONSTRAINT fk_to_employee_id
    FOREIGN KEY (employee_id)
    REFERENCES employees(employee_id);

ALTER TABLE `job_history`
    ADD CONSTRAINT fk_to_job_id2
    FOREIGN KEY (job_id)
    REFERENCES jobs(job_id);

ALTER TABLE `locations`
    ADD CONSTRAINT fk_to_country_id
    FOREIGN KEY (country_id)
    REFERENCES countries(country_id);
